<?php

namespace Jyrmo\Store\Pdo;

use Jyrmo\Store\{StoreInterface, Entity};
use Jyrmo\Store\Pdo\Exception\PdoStoreException;

class PdoStore implements StoreInterface {
    /**
     * @var \PDO
     */
    protected $pdo;

    /**
     * @var SelectStatementBuilder
     */
    protected $selectStatementBuilder;

    /**
     * @var InsertStatementBuilder
     */
    protected $insertStatementBuilder;

    /**
     * @var UpdateStatementBuilder
     */
    protected $updateStatementBuilder;

    /**
     * @var string
     */
    protected $tableName;

    /**
     * @var string
     */
    protected $entityClassName = 'Jyrmo\Store\Entity';

    protected function getErrorMsg() : string {
        $arrInfo = $this->pdo->errorInfo();
        $errorMsg = $arrInfo[2];

        return (string)$errorMsg;
    }

    protected function executeSqlStatement(SqlStatement $sqlStmt) : \PDOStatement {
        // TODO: Factor out to query runner class.

        $sql = $sqlStmt->getSql();
        $pdoStmt = $this->pdo->prepare($sql);
        if (!$pdoStmt) {
            $errorMsg = $this->getErrorMsg();
            throw new PdoStoreException('Could not prepare query: ' . $errorMsg);
        }
        $substitutions = $sqlStmt->getSubstitutions();
        $isSuccess = $pdoStmt->execute($substitutions);
        if (!$isSuccess) {
            $errorMsg = $this->getErrorMsg();
            throw new PdoStoreException('Could not execute query: ' . $errorMsg);
        }

        return $pdoStmt;
    }

    protected function fetchOne(SqlStatement $sqlQuery, string $entityClassName = null) : Entity {
        $entityClassName = $entityClassName ?: $this->entityClassName;
        $pdoStmt = $this->executeSqlStatement($sqlQuery);
        $entity = $pdoStmt->fetchObject($entityClassName);
        if (!$entity) {
            throw new PdoStoreException('Could not fetch entity from database.');
        }

        return $entity;
    }

    protected function fetchAll(SqlStatement $sqlQuery, string $entityClassName = null) : array {
        $entityClassName = $entityClassName ?: $this->entityClassName;
        $pdoStmt = $this->executeSqlStatement($sqlQuery);
        $entities = $pdoStmt->fetchAll(\PDO::FETCH_CLASS, $entityClassName);

        return $entities;
    }

    protected function fetchColumn(SqlStatement $sqlQuery) {
        $pdoStmt = $this->executeSqlStatement($sqlQuery);
        $result = $pdoStmt->fetchColumn();

        return $result;
    }

    public function setPdo(\PDO $pdo) {
        $this->pdo = $pdo;
    }

    public function setTableName(string $tableName) {
        $this->tableName = $tableName;
    }

    public function setEntityClassName(string $entityClassName) {
        // TODO: assert entity class extends Entity.
        $this->entityClassName = $entityClassName;
    }

    public function __construct() {
        $this->selectStatementBuilder = new SelectStatementBuilder();
        $this->insertStatementBuilder = new InsertStatementBuilder();
        $this->updateStatementBuilder = new UpdateStatementBuilder();
    }

    public function getByKey($key) : Entity {
        $arrWhere = array('id' => $key);
        $selectStmt = $this->selectStatementBuilder->build($this->tableName, $arrWhere);
        $entity = $this->fetchOne($selectStmt);

        return $entity;
    }

    public function query($query) {
        // TODO: sql string query.
        // TODO: SqlStatement query.
        // TODO: assuming query is array.

        $selectStmt = $this->selectStatementBuilder->build($this->tableName, $query);
        $entities = $this->fetchAll($selectStmt);

        return $entities;
    }

    public function queryOne($query) : Entity {
        $selectStmt = $this->selectStatementBuilder->build($this->tableName, $query);
        $entity = $this->fetchOne($selectStmt);

        return $entity;
    }

    public function insert(Entity $entity) {
        // TODO: transaction.

        $sqlStmt = $this->insertStatementBuilder->build($this->tableName, $entity);
        $this->executeSqlStatement($sqlStmt);
        $entity->id = $this->pdo->lastInsertId();
    }

    public function update(Entity $entity) {
        $sqlStmt = $this->updateStatementBuilder->build($this->tableName, $entity);
        $this->executeSqlStatement($sqlStmt);
    }

    public function save(Entity $entity) {
        if ($entity->id === null) {
            $this->insert($entity);
        } else {
            $this->update($entity);
        }
    }

    public function deleteByKey($key) {
        $sqlStmt = new SqlStatement();
        $sql = 'DELETE FROM `' . $this->tableName . '` WHERE `id` = :id';
        $sqlStmt->setSql($sql);
        $substitutions = array(':id' => $key);
        $sqlStmt->setSubstitutions($substitutions);
        $this->executeSqlStatement($sqlStmt);
    }

    public function delete(Entity $entity) {
        $this->deleteByKey($entity->id);
    }
}
