<?php

namespace Jyrmo\Store\Pdo;

class SelectStatementBuilder {
    protected function buildWhereClause(array $arrWhere) : string {
        $whereConditions = array();
        foreach ($arrWhere as $field => $val) {
            $whereConditions[] = $field . ' = :' . $field;
        }
        $whereClause = join(' AND ', $whereConditions);

        return $whereClause;
    }

    protected function buildSql(string $tableName, array $arrWhere) {
        // TODO: Tablename substitution conundrum.

        $whereClause = $this->buildWhereClause($arrWhere);
        $sql = 'SELECT * FROM `' . $tableName . '` WHERE ' . $whereClause;

        return $sql;
    }

    protected function buildSubstitutions(array $arrWhere) : array {
        $substitutions = array();
        foreach ($arrWhere as $field => $val) {
            $substitutions[':' . $field] = $val;
        }

        return $substitutions;
    }

    public function build(string $tableName, array $arrWhere) : SqlStatement {
        $sqlStmt = new SqlStatement();
        $sql = $this->buildSql($tableName, $arrWhere);
        $sqlStmt->setSql($sql);
        $substitutions = $this->buildSubstitutions($arrWhere);
        $sqlStmt->setSubstitutions($substitutions);

        return $sqlStmt;
    }
}
