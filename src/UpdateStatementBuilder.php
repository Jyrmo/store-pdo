<?php

namespace Jyrmo\Store\Pdo;

use Jyrmo\Store\Entity;

class UpdateStatementBuilder {
    // TODO: SqlStatementBuilder interface.

    protected function entityToArr(Entity $entity) : array {
        // TODO: Factor out to entity helper class or entity class.

        $arr = get_object_vars($entity);

        return $arr;
    }

    protected function buildSetClause(Entity $entity) : string {
        $arrEntity = $this->entityToArr($entity);
        $setPhrases = array();
        foreach ($arrEntity as $field => $val) {
            $setPhrases[] = '`' . $field . '` = :' . $field;
        }
        $setClause = join(', ', $setPhrases);

        return $setClause;
    }

    protected function buildSql(string $tableName, Entity $entity) : string {
        $setClause = $this->buildSetClause($entity);
        $sql = 'UPDATE `' . $tableName . '` SET ' . $setClause . ' WHERE `id` = :id';

        return $sql;
    }

    protected function buildSubstitutions(Entity $entity) : array {
        $fields = $this->entityToArr($entity);
        $substitutions = array();
        foreach ($fields as $field => $val) {
            $substitutions[':' . $field] = $val;
        }

        return $substitutions;
    }

    public function build(string $tableName, Entity $entity) : SqlStatement {
        $sqlStmt = new SqlStatement();
        $sql = $this->buildSql($tableName, $entity);
        $sqlStmt->setSql($sql);
        $substitutions = $this->buildSubstitutions($entity);
        $sqlStmt->setSubstitutions($substitutions);

        return $sqlStmt;
    }
}
