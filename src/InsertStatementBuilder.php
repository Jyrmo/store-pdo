<?php

namespace Jyrmo\Store\Pdo;

use Jyrmo\Store\Entity;

class InsertStatementBuilder {
    protected function entityToArr(Entity $entity) : array {
        // TODO: Factor out to entity helper class or entity class.

        $arr = get_object_vars($entity);

        return $arr;
    }

    protected function getEntityFields(Entity $entity) : array {
        $arrEntity = $this->entityToArr($entity);
        $fields = array_keys($arrEntity);

        return $fields;
    }

    protected function buildFieldsClause(Entity $entity) : string {
        // TODO: possibly use Pdo quote function.

        $fields = $this->getEntityFields($entity);
        $quote = function(string $val) : string {
            $quotedVal = '`' . $val . '`';

            return $quotedVal;
        };
        $quotedFields = array_map($quote, $fields);
        $fieldsClause = join(', ', $quotedFields);

        return $fieldsClause;
    }

    protected function buildValuesClause(Entity $entity) : string {
        $fields = $this->getEntityFields($entity);
        $mark = function(string $val) : string {
            $markedVal = ':' . $val;

            return $markedVal;
        };
        $markedFields = array_map($mark, $fields);
        $valuesClause = join(', ', $markedFields);

        return $valuesClause;
    }

    protected function buildSql(string $tableName, Entity $entity) : string {
        $fieldsClause = $this->buildFieldsClause($entity);
        $valuesClause = $this->buildValuesClause($entity);
        $sql = 'INSERT INTO `' . $tableName . '` (' . $fieldsClause . ') VALUES (' . $valuesClause . ')';

        return $sql;
    }

    protected function buildSubstitutions(Entity $entity) : array {
        $fields = $this->entityToArr($entity);
        $substitutions = array();
        foreach ($fields as $field => $val) {
            $substitutions[':' . $field] = $val;
        }

        return $substitutions;
    }

    public function build(string $tableName, Entity $entity) : SqlStatement {
        $sqlStmt = new SqlStatement();
        $sql = $this->buildSql($tableName, $entity);
        $sqlStmt->setSql($sql);
        $substitutions = $this->buildSubstitutions($entity);
        $sqlStmt->setSubstitutions($substitutions);

        return $sqlStmt;
    }
}
