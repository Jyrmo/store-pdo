<?php

namespace Jyrmo\Store\Pdo;

class SqlStatement {
    // TODO: account for idiosyncrasies with array key types and what not.

    /**
     * @var string
     */
    protected $sql;

    /**
     * @var array
     */
    protected $substitutions = array();

	public function setSql(string $sql) {
		$this->sql = $sql;
	}
    
	public function getSql() : string {
    	return $this->sql;
	}

	public function setSubstitutions(array $substitutions) {
		$this->substitutions = $substitutions;
	}

	public function getSubstitutions() : array {
    	return $this->substitutions;
	}
}
